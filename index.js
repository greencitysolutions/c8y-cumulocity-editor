const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const request = require('request');
const async = require('async');
const URL = require('url');
const querystring = require('querystring');



const c8y_url = process.argv[2]
if (!c8y_url) {
}


app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  let auth = req.query.auth;
  let type = req.query.type;
  let fragmentType = req.query.fragmentType;
  let from = req.query.from||'1970-01-01';
  let to = req.query.to||new Date(Date.now()).toISOString().split('T')[0];
  let source = req.query.source;
  let pageSize = req.query.pageSize||50;
  let pageNumber = req.query.pageNumber||1;

  if (auth && (type || fragmentType || from || to || source)) {
    const headers = { 
      Authorization: `Basic ${auth}`,
      Charset: 'utf-8',
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'Accept-Charset': 'utf-8'
    }
    let queryOptions = {
      revert: true,
      dateFrom: from + 'T00:00:00.000Z',
      dateTo: to + 'T23:59:59.000Z',
      pageSize,
      withTotalPages: true,
      currentPage: pageNumber
    }
    if (source) queryOptions.source = source;
    if (fragmentType) queryOptions.fragmentType = fragmentType;
    if (type) queryOptions.type = type;
    // let uri = `${c8y_url}/measurement/measurements?source=${source}&revert=true&dateFrom=${from}&dateTo=${to}&pageSize=${pageSize}&withTotalPages=true&currentPage=${pageNumber}`;
    let uri = `${c8y_url}/measurement/measurements?${querystring.stringify(queryOptions)}`;
    console.log('uri', uri)
    // console.log('auth', auth)

    let paginatorQueryTemplate = '?' + URL.parse(req.url).query.replace(/pageNumber\=\d+/, 'pageNumber=PAGE_NUMBER');
    // console.log('paginatorQueryTemplate', paginatorQueryTemplate)

    request.get({uri, headers}, (error, response, body) => {
      if (response.statusCode != 200) return res.json(JSON.parse(body));
      body = JSON.parse(body);
      // console.dir(body, {depth:null});
      let measurements = body.measurements;
      let pageCount = body.statistics.totalPages;
      res.render('index', {auth, type, fragmentType, from, to, source, pageSize, pageNumber, pageCount, paginatorQueryTemplate, measurements});
    });
  } else {
    res.render('index', {auth, type, fragmentType, from, to, source, pageSize, pageNumber, measurements:[]});
  }
});

app.post('/delete', (req, res) => {
  let ids = Object.keys(req.body);
  let authIndex = ids.indexOf('auth');
  if (authIndex > -1) {
    ids.splice(authIndex, 1);
  }
  console.log(`Deleting ${JSON.stringify(ids)}`);

  const headers = { 
    Authorization: `Basic ${req.body.auth}`,
    Charset: 'utf-8',
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Accept-Charset': 'utf-8'
  }

  async.eachLimit(ids, 4, (id, cb) => { 
    let uri = `${c8y_url}/measurement/measurements/${id}`;
    console.log('delete', uri);
    request.delete({uri, headers}, ((error, response, body) => {
      if (error) throw error;
      console.log(id, response.statusCode, body)
      return cb();
    }));
  },
  error => {
    if (error) throw error;
    res.redirect(req.get('Referrer'));
  });

});

app.use('/static/bootstrap', express.static('node_modules/bootstrap/dist'));
app.use('/static/popper',    express.static('node_modules/popper.js/dist'));
app.use('/static/jquery',    express.static('node_modules/jquery/dist'   ));
app.use('/static/custom',    express.static('static'                     ));

app.listen(3000, () => console.log('Listening on port 3000!'));